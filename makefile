#if it doesn't compile run: 
#sudo apt-get install libboost-all-dev
#then try again

@PHONY = clean;

NAME = shell
CXX = g++-4.7
RELFLAGS = -g0 -Wall -O3 -pedantic --std=c++11
CPPFLAGS = -g3 -O0 -Wextra -Wall -Werror --std=c++11 

$(NAME): main.o
	$(CXX) $(CPPFLAGS) -o $@ $^

%.o: %.cpp %.hpp
	$(CXX) $(CPPFLAGS) $^ -c 

release: main.o
	$(CXX) $(RELFLAGS) -o $@ $^

clean:
	rm *.o
	rm $(NAME)
	rm *.gch
	#rm release
