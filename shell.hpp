#include <string>
#include <iostream>
#include <iomanip>
#include <boost/algorithm/string.hpp>
#include "executor.hpp"

class Shell {
private:
	Executor executor;
	History history;

public:
	void getInput()
	{
		bool cont = true;
		while(cont)
		{
			std::string command;
			std::cout << "> ";
			std::getline(std::cin, command);
			cont = processInput(command);
		}
		return;
	}

	bool processInput(std::string command)
	{
		std::vector<std::string> results;
		std::string newCommand;
		boost::split(results, command, boost::is_any_of(" "));
		if(results[0] == "exit" || results[0] == "quit")
		{
			//return false;
			exit(0);
		}
		else if(results[0] == "^" && (int)results.size() > 1)
		{
			results = history.getPrevCmd(std::stoi(results[1]));
			executor.runCmd(results);
		}
		else if(results[0] == "!")
		{
			results = history.getLastCmd();
			executor.runCmd(results);
		}
		else if(results[0] == "history")
		{
			history.printHistory();
		}
		else if(results[0] == "ptime")
		{
			executor.printTime();
		}
		else
		{
			executor.runCmd(results);
		}
		history.storeCmd(results);
		return true;
	}
};