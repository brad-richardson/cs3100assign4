#include <vector>
#include <string>
#include <iostream>
#include <iomanip>

class History
{
private:
	std::vector <std::vector<std::string>> hist;
	std::vector <std::string> EMPTYVECTOR;

public:
	//for '^ #' command
	std::vector<std::string> getPrevCmd(int id)
	{
		if(id > (int)hist.size())
		{
			return EMPTYVECTOR;
		}
		return hist[id-1];
	}

	//for use with '!' command
	std::vector<std::string> getLastCmd()
	{
		if((int)hist.size() == 0)
		{
			return EMPTYVECTOR;
		}
		return hist[hist.size()-1];
	}

	void storeCmd(std::vector<std::string> command)
	{
		hist.push_back(command);
	}

	void printHistory()
	{
		if((int)hist.size() == 0)
		{
			std::cout << "No previous commands" << std::endl;
			return;
		}
		int count = 1;
		for(auto cmd : hist)
		{
			std::cout << count << ": ";
			for(auto arg : cmd)
			{
				std::cout << arg << " ";
			}
			std::cout << std::endl;
			++count;
		}
	}

	History()
	{
		EMPTYVECTOR.push_back("");
	}
};