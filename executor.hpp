#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <unistd.h>//for forking and execv
#include <sys/wait.h> //for waitpid
#include <chrono>
#include "history.hpp"

class Executor 
{
private:
	int prevSeconds = 0;
	int prevMilliseconds = 0;
	int prevMicroseconds = 0;

public:
	void runCmd(std::vector<std::string> toRun)
	{
		auto start = std::chrono::system_clock::now();
		auto pid = fork();
		int status = 0;
		if(pid == -1)
		{
			std::cerr << "Error while forking...";
		}
		else if(pid == 0)
		{
			//child process - does commands
			//creates an array of c strings to pass to execvp
			char * args[toRun.size()+1];
			args[0] = (char*)toRun[0].c_str();
			if((int)toRun.size() > 1)
			{
				for (int i = 1; i < (int)toRun.size(); ++i)
				{
					args[i] = (char*)toRun[i].c_str();
				}
			}
			args[toRun.size()] = NULL;
			execvp(args[0], args);
		}
		else
		{
			//parent - just waits for child process to finish
			waitpid(pid, &status, 0);
		}
		auto end = std::chrono::system_clock::now();
		prevSeconds = std::chrono::duration_cast<std::chrono::seconds>(end-start).count();
		prevMilliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
		prevMicroseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	}

	void printTime()
	{
		std::cout << "Time to process previous command: " << std::endl;
		std::cout << "Seconds: " << prevSeconds;
		std::cout << " Microseconds: " << prevMilliseconds;
		std::cout << " Microseconds: " << prevMicroseconds;
		std::cout << std::endl;
	}
};